
/**
 * Some Tests for filtering to fastest and cheapest
 */
// fastest.forEach((v, key) => {
//   console.log("*******************")
//   console.log(key)
//   console.log("*******************")
//   console.log("Fastest: ", v.transport)
//   console.log("cheapest: ", cheapest.get(key).transport)
// })


class TravelSuggester {
  options;
  optionsGraph;
  suggestedPaths;
  cheapest = new Map();
  fastest = new Map();

  constructor(options) {
    this.options = options;
    this.optionsGraph = {};
    this.suggestedPaths = [];
    this.categorizeDeals();
    this.representDealsGraph();
  }


  /**
 * Get the best result from all suggested paths
 * @param {string} suggestBy 
 */
  findBestSuggestion(from, to, suggestBy = "cheapest") {
    if (!this.optionsGraph[from]) {
      throw new Error("Invalid 'from' option")
    }

    if (!this.optionsGraph[to]) {
      throw new Error("Invalid 'to' option")
    }

    this.suggestedPaths = [];
    this.extractAllValidPaths(from, to);

    let bestSuggestion = this.suggestedPaths.reduce((best, nextPath, idx) => {
      let stations = [], option, totalCost = 0, totalDuration = 0;

      for (let i = 0; i < nextPath.length; i++) {
        const from = nextPath[i];
        const to = nextPath[i + 1];
        if (to) {
          if (suggestBy === 'cheapest') {
            const tripDetails = this.cheapest.get(`${from} to ${to}`);
            // Accumulate cost & duration for route
            totalCost += tripDetails.cost - ((tripDetails.cost * tripDetails.discount) / 100);
            totalDuration += (parseInt(tripDetails.duration.h) * 60) + parseInt(tripDetails.duration.m);
            stations.push(tripDetails);
          } else {
            const tripDetails = this.fastest.get(`${from} to ${to}`);
            // Accumulate cost & duration for route
            totalCost += tripDetails.cost - ((tripDetails.cost * tripDetails.discount) / 100);
            totalDuration += (parseInt(tripDetails.duration.h) * 60) + parseInt(tripDetails.duration.m);
            stations.push(tripDetails);
          }
        }
      }

      if (idx === 0) {
        best = {
          route: stations,
          cost: totalCost,
          duration: totalDuration
        };

        // console.log("Initial best by: ", suggestBy);
      } else {
        if (suggestBy === 'cheapest') {
          if (totalCost < best.cost) {
            best = { route: stations, cost: totalCost, duration: totalDuration };
            // console.log("Best Cost: ", best.cost);
          } else if (totalCost == best.cost && totalDuration < best.duration) {
            best = { route: stations, cost: totalCost, duration: totalDuration };
            // console.log("Same cost but better duration: ", best.duration);
          }
        } else {
          if (totalDuration < best.duration) {
            best = { route: stations, cost: totalCost, duration: totalDuration };
            // console.log(`Better path:   cost: ${best.cost},  duration: ${best.duration}`);
          } else if (totalDuration == best.duration && totalCost < best.cost) {
            best = { route: stations, cost: totalCost, duration: totalDuration };
            // console.log("Same duration but better Cost: ", best.cost);
          }
        }
      }

      return best;
    }, { route: [], cost: 0, duration: 0 });

    const totalHours = Math.floor(bestSuggestion.duration / 60).toString().padStart(2, '0');
    const totalMinuts = (bestSuggestion.duration % 60).toString().padStart(2, '0');

    bestSuggestion = {...bestSuggestion, duration: `${totalHours}h${totalMinuts}`}

    return bestSuggestion;
  }

  /**
   * Categorize Deals by cheapest/fastest
   */
  categorizeDeals() {
    this.options.forEach(deal => {
      const dealTitle = `${deal.departure} to ${deal.arrival}`;

      deal = { ...deal, costAfterDiscount: deal.cost - (deal.cost * deal.discount / 100) };
      // Filterd to fastest
      if (!this.fastest.has(dealTitle)) {
        this.fastest.set(dealTitle, deal);
      } else {
        const oldDeal = this.fastest.get(dealTitle);
        const currentDealDuration = (parseInt(deal.duration.h) * 60) + parseInt(deal.duration.m);
        const oldDealDuration = (parseInt(oldDeal.duration.h) * 60) + parseInt(oldDeal.duration.m);
        if (currentDealDuration < oldDealDuration) {
          // Replace with faster one
          this.fastest.set(dealTitle, deal);
        }
      }

      // Filterd to cheapest
      if (!this.cheapest.has(dealTitle)) {
        this.cheapest.set(dealTitle, deal);
      } else {
        const oldDeal = this.cheapest.get(dealTitle);
        const currentDealCost = deal.cost - (deal.cost * deal.discount / 100);
        const oldDealCost = oldDeal.cost - (oldDeal.cost * oldDeal.discount / 100);
        if (currentDealCost < oldDealCost) {
          // Replace with faster one
          this.cheapest.set(dealTitle, deal);
        }
      }
    });
  }


  /**
   * Represent options graph
   */
  representDealsGraph() {
    this.options.forEach((option) => {
      if (!this.optionsGraph[option.departure]) {
        this.optionsGraph[option.departure] = [];
      }
      if (!this.optionsGraph[option.departure].includes(option.arrival))
        this.optionsGraph[option.departure].push(option.arrival);
    });
  }


  /**
   * 
   * @param {string} from // the point you want to travel from
   * @param {string} to // The point you want to travel to
   * @param {Set<strings>} isVisited // Keep track of all visited city during the deep search
   * @param {string[]} localPathList // Keep caching the path until reaching a the valid destination
   */
  extractAllValidPaths(from, to, isVisited = new Set(), localPathList = []) {
    if (!localPathList.length) {
      localPathList.push(from);
    }
    if (from === to) {
      this.suggestedPaths.push([...localPathList])
      return;
    }

    // Mark the current node
    isVisited.add(from)

    if (this.optionsGraph[from]) {
      this.optionsGraph[from].forEach((city) => {
        if (!isVisited.has(city)) {
          localPathList.push(city);
          this.extractAllValidPaths(city, to, isVisited, localPathList);
          localPathList.pop();
        }
      });
    }

    // Mark the current node
    isVisited.delete(from)
  }
}


module.exports = TravelSuggester
