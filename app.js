const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors')
const travels = require('./routes/travel');

const app = express();

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser())

app.use('/api/travel', travels);


/**
 * Error Handler
 */
app.use(function (err, req, res, next) {
    res.status(500).send(err.message)
})

module.exports = app;
