const express = require('express');
const router = express.Router();
const TravelSuggester = require('../travel/travel-suggester');
const { deals, cities } = require('../data');
const suggester = new TravelSuggester(deals)

/* GET users listing. */
router
  .get('/', async (req, res, next) => {
    try {
      const { from, to, suggestedBy = 'cheapest' } = req.query;
      const bestSuggestion = suggester.findBestSuggestion(from, to, suggestedBy);
      res.json(bestSuggestion);
    } catch (error) {
      next(error)
    }
  })
  .get('/cities', (req, res, next) => {
    res.json([...cities]);
  })

module.exports = router;
